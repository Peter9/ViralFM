use async_trait::async_trait;

use std::fs::File;
use std::io::{BufReader};
use std::sync::{Arc, Mutex};
use std::sync::mpsc::SyncSender;
use std::thread;
use std::time;
use tokio::sync::mpsc;


use super::application::{PlayerCallback, PlayerControl};
use rodio::DeviceTrait;
use rodio::Sink;

pub struct RodioPlayerEvents {
    rx_events: mpsc::UnboundedReceiver<PlayerEvent>,
}

pub struct RodioPlayerControl {
    tx_commands: SyncSender<PlayerCommand>,
}

pub enum PlayerCommand {
    Next(String),
    PauseUnpause,
    Quit,
}

#[derive(Debug)]
pub enum PlayerEvent {
    CorruptTrack(String),
    EndOfTrack,
}

impl RodioPlayerControl {
    pub fn new_events_and_control() -> (RodioPlayerEvents, RodioPlayerControl) {
        print!("Output device: {}\r\n", match rodio::default_output_device().unwrap().name() {
           Ok(name) => name,
           Err(_) => "Unknown".to_string(),
        });

        let (tx_events, rx_events) = tokio::sync::mpsc::unbounded_channel::<PlayerEvent>();
        let (tx_commands, rx_commands) = std::sync::mpsc::sync_channel::<PlayerCommand>(16);
        std::thread::spawn(move || {
            let device = rodio::default_output_device().unwrap();
            let mut sink = Sink::new(&device);
            let mut in_progress = false;
            let mut paused = false;
            loop {
                if in_progress && sink.empty() {
                    tx_events.send(PlayerEvent::EndOfTrack).unwrap();
                    in_progress = false;
                } else {
                    match rx_commands.try_recv() {
                        Err(_) => thread::sleep(time::Duration::from_millis(100)),
                        Ok(command) => match command {
                            PlayerCommand::PauseUnpause => { 
                                if paused {
                                    sink.play();
                                    paused = false;
                                } else {
                                    sink.pause();
                                    paused = true;
                                }
                            },
                            PlayerCommand::Next(filename) => { 
                                println!("Filename: {}\r", filename);
                                sink = Sink::new(&device);
                                match File::open(filename.to_owned()) {
                                    Ok(file) => {
                                        match rodio::Decoder::new(BufReader::new(file)) {
                                            Ok(source) => {
                                                sink.append(source);
                                                sink.play();
                                                in_progress = true;
                                            }
                                            Err(rodio::decoder::DecoderError::UnrecognizedFormat) => {
                                                tx_events.send(PlayerEvent::CorruptTrack(filename)).unwrap();
                                            }
                                        }
                                        paused = false;
                                    },
                                    Err(msg) => {
                                        println!("{}\r", msg);
                                    }
                                }
                            },
                            PlayerCommand::Quit => { return; },
                        }
                    }
                }
            }
        });
        return (
          RodioPlayerEvents {
              rx_events: rx_events,
          },
          RodioPlayerControl {
            tx_commands: tx_commands,
          }, 
        );
    }
}

fn handle_event<C: PlayerCallback>(callback: Arc<Mutex<C>>, event: PlayerEvent) {
    match event {
        PlayerEvent::CorruptTrack(filename) => { callback.lock().unwrap().track_is_corrupt(filename); },
        PlayerEvent::EndOfTrack  => { callback.lock().unwrap().track_ended();},
    }
}

impl RodioPlayerEvents {
    pub async fn run<C: PlayerCallback>(&mut self, callback: Arc<Mutex<C>>) {
        match self.rx_events.recv().await {
            Some(event) => { 
                handle_event(callback, event);
                return; 
             },
            None        => { panic!("Player channel dropped!"); },
        }
    }
}

#[async_trait]
impl PlayerControl for RodioPlayerControl {
    fn next(&self, track_path: String) {
        self.tx_commands.send(PlayerCommand::Next(track_path)).unwrap();
    }

    fn pause_or_unpause(&self) {
        self.tx_commands.send(PlayerCommand::PauseUnpause).unwrap();
    }

    fn quit(&self) {
        self.tx_commands.send(PlayerCommand::Quit).unwrap();
    }
}

