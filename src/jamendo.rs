use std::cmp;
use std::fs::File;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::SyncSender;
use std::thread;
use std::time;
use tokio::sync::mpsc;

use rand::Rng;
use ureq;

use super::application::{Downloader, DownloaderCallback, JamendoTrackInfo};

pub struct JamendoDownloader {
    tx_commands: SyncSender<DownloaderCommand>,
}

pub struct JamendoDownloaderEvents {
    rx_events: mpsc::UnboundedReceiver<DownloaderEvent>,
}

#[derive(Debug)]
pub enum DownloaderEvent {
    TrackFound(JamendoTrackInfo),
    TrackDownloaded(JamendoTrackInfo, String),
    FindTrackError,
}

impl JamendoDownloader {
    pub fn new_events_and_control() -> (JamendoDownloaderEvents, JamendoDownloader) {
        let (tx_events, rx_events) = tokio::sync::mpsc::unbounded_channel::<DownloaderEvent>();
        let (tx_commands, rx_commands) = std::sync::mpsc::sync_channel::<DownloaderCommand>(16);
        std::thread::spawn(move || {
            loop {
                match rx_commands.try_recv() {
                    Err(_) => thread::sleep(time::Duration::from_millis(1000)),
                    Ok(command) => match command {
                        DownloaderCommand::FindTrack => {
                            match get_random_track() {
                                Ok(track) => {
                                    tx_events.send(DownloaderEvent::TrackFound(track)).unwrap();
                                },
                                Err(_) => {
                                    tx_events.send(DownloaderEvent::FindTrackError).unwrap();
                                }
                            }
                        },
                        DownloaderCommand::DownloadTrack(track, local_path) => {
                            download_track(&track.url, &local_path);
                            tx_events.send(DownloaderEvent::TrackDownloaded(track, local_path)).unwrap();
                        }
                    }
                }
            }
        });
        return (
            JamendoDownloaderEvents {
                rx_events,
            },
            JamendoDownloader {
                tx_commands,
            }
        );
    }
}

enum DownloaderCommand {
    FindTrack,
    DownloadTrack(JamendoTrackInfo, String),
}

impl JamendoDownloaderEvents {
    pub async fn run<C: DownloaderCallback>(&mut self, callback: Arc<Mutex<C>>) {
        loop {
            match self.rx_events.recv().await {
                Some(DownloaderEvent::TrackFound(track)) => { callback.lock().unwrap().track_found(track); },
                Some(DownloaderEvent::TrackDownloaded(track, full_path)) => { callback.lock().unwrap().track_downloaded(track, full_path); },
                Some(DownloaderEvent::FindTrackError) => { println!("Failed attempt to download through Jamendo API!\r\n"); },
                None => { panic!("Downloader disconnect!"); },
            }
        }
    }
}

impl Downloader for JamendoDownloader {
    fn find_track_to_download(&self) {
        self.tx_commands.send(DownloaderCommand::FindTrack).unwrap();
    }

    fn download_track(&self, track: JamendoTrackInfo, local_path: String) {
        self.tx_commands.send(DownloaderCommand::DownloadTrack(track, local_path)).unwrap();
    }
}

fn download_track(url: &String, full_path: &str) {
    let resp = ureq::get(url.as_str()).call();
    let mut web_reader = resp.into_reader();
    let mut file = File::create(&full_path).unwrap();
    match std::io::copy(&mut web_reader, &mut file) {
        Ok(_) => (),
        Err(_) => println!("Problem copying files"),
    }
}

pub fn get_random_track() -> Result<JamendoTrackInfo,()> {
    const MAX_OFFSET: u32 = 76000;
    let track_no = cmp::min(rand::thread_rng().gen_range(1, MAX_OFFSET), rand::thread_rng().gen_range(1, MAX_OFFSET));
    let url = format!("https://api.jamendo.com/v3.0/tracks?client_id=16cbb905&order=listens_week&limit=1&offset={}", track_no);
    let resp = ureq::get(url.as_str()).call();
    if resp.ok() { 
        let json = resp.into_json().ok().ok_or(())?;
        let _obj = json.as_object().ok_or(())?;
        let status = _obj["headers"].as_object().ok_or(())?["status"].as_str().ok_or(())?;
        if status == "success" {
            let results = _obj["results"].as_array().ok_or(())?;
            let result = results[0].as_object().ok_or(())?;
            let artist = result["artist_name"].as_str().ok_or(())?;
            let title = result["name"].as_str().ok_or(())?;
            let url = result["audiodownload"].as_str().ok_or(())?;
            let id = result["id"].as_str().ok_or(())?;
            let jamendo_id: u32 = id.parse().ok().ok_or(())?;
            return Ok(JamendoTrackInfo {
                artist: artist.to_string(),
                title: title.to_string(),
                url: url.to_string(),
                jamendo_id: jamendo_id,
                file_name: format!("jamendo{}.mp3", jamendo_id),
            });
        } else {
            Err(())
        }
    } else {
        Err(())
    }
}

