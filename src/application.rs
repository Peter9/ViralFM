use async_trait::async_trait;

use std::path::Path;
use std::sync::{Arc};

use super::catalog;
use super::catalog::{Track};

pub trait UserInterfaceContext {
    fn drop_current_track(&mut self);
    fn play_next_track(&mut self);
    fn pause_or_unpause(&mut self);
    fn quit(&mut self);
}

#[async_trait]
pub trait PlayerControl : Send {
    fn next(&self, track_path: String);
    fn pause_or_unpause(&self);
    fn quit(&self);
}

#[async_trait]
pub trait UserInterfaceControl: Send {
    fn downloading_file(&self, track: &JamendoTrackInfo, full_name: &String);
    fn now_playing(&self, track: &Track);
}

pub trait PlayerCallback : Send {
    fn track_ended(&mut self);
    fn track_is_corrupt(&mut self, file_name: String); 
}

pub trait Downloader : Send {
    fn find_track_to_download(&self);
    fn download_track(&self, track: JamendoTrackInfo, local_path: String);
}

pub trait DownloaderCallback : Send {
    fn track_downloaded(&mut self, track: JamendoTrackInfo, full_path: String);
    fn track_found(&mut self, track: JamendoTrackInfo);
}

#[derive(Debug)]
pub struct JamendoTrackInfo {
    pub jamendo_id: u32,
    pub artist: String,
    pub title: String,
    pub url: String,
    pub file_name: String,
}

#[derive(Debug)]
pub enum DownloaderEvent {
    TrackFound(JamendoTrackInfo),
    TrackDownloaded(JamendoTrackInfo, String),
    FindTrackError,
}

pub struct ApplicationContext {
    pub catalog: catalog::Catalog,
    pub current: Option<Arc<Track>>,
    pub running: bool,
    pub downloading: bool, 
    pub player_control: Box<dyn PlayerControl>,
    pub ui_control: Box<dyn UserInterfaceControl>,
    pub downloader: Box<dyn Downloader>,
    pub music_dir: String,
}

impl UserInterfaceContext for ApplicationContext {
    fn drop_current_track(&mut self) {
        match &self.current {
            Some(track) => {
                self.catalog.remove_track_from_rotation(&track).unwrap();
            }
            None => (),
        }
        self.play_next_track();
        self.download_if_needed();
    }

    fn play_next_track(&mut self) {
        self.play_random_track();
    }

    fn pause_or_unpause(&mut self) {
        self.player_control.pause_or_unpause();
    } 

    fn quit(&mut self) {
        self.running = false;
    }
}

impl PlayerCallback for ApplicationContext {
    fn track_is_corrupt(&mut self, file_name: String) {
        self.handle_corrupt_track(file_name); 
        self.play_next_track();
        self.download_if_needed();
    }

    fn track_ended(&mut self) {
        self.play_next_track();
    }
}

impl DownloaderCallback for ApplicationContext {
    fn track_downloaded(&mut self, track: JamendoTrackInfo, full_path: String) {
        self.catalog.add_track_to_catalog_and_rotation(track.artist, track.title, full_path).unwrap();
        self.downloading = false;
        self.download_if_needed();
    }

    fn track_found(&mut self, track: JamendoTrackInfo) {
        if self.catalog.contains_jamendo_track(track.jamendo_id) {
            self.downloading = false;
            self.download_if_needed();
        } else {
            let file_name = format!("jamendo{}.mp3", track.jamendo_id);
            let path = Path::new(&self.music_dir).join(file_name);
            let full_path = path.to_str().unwrap().to_string();
            self.ui_control.downloading_file(&track, &full_path);
            self.downloader.download_track(track, full_path);
        }
    }
}

impl ApplicationContext {
    fn handle_corrupt_track(&mut self, file_path: String) {
        match &self.current {
            Some(track) => {
                if track.full_path().eq(&file_path) {
                    self.catalog.remove_track_from_rotation(&track).unwrap();

                }
            }
            None => (),
        }
    }

    pub fn play_random_track(&mut self) {
        if self.catalog.get_default_rotation().number_of_tracks() > 0 {
          let now_playing = self.catalog.random_track();
          self.ui_control.now_playing(&now_playing);
          self.player_control.next(now_playing.full_path());
          self.current = Some(now_playing);
        }
    }

    fn download_if_needed(&mut self) {
        if !self.downloading && self.catalog.get_default_rotation().number_of_tracks() < 150 {
            self.downloader.find_track_to_download(); 
            self.downloading = true;
        }
    }
}
