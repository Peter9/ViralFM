use indexmap::set::IndexSet;
use rand::Rng;
use rusqlite::{Connection};
use rusqlite::NO_PARAMS;
use std::cmp::{Eq, PartialEq};
use std::collections::HashMap;
use std::error::Error;
use std::sync::Arc;
use std::hash::{Hash, Hasher};

use super::sqlite;

pub struct Track {
    id: u32,
    artist: String,
    title: String,
    file_path: String, 
    jamendo_id: u32,
}

impl Track {
    pub fn new(id: u32, artist: String, title: String, file_path: String) -> Track {
        return Track {
            id,
            artist,
            title,
            file_path,
            jamendo_id: 0,
        }
    }

    pub fn display(&self) -> String {
        return format!("{} - {}", self.artist, self.title);
    }


    pub fn full_path(&self) -> String {
        return self.file_path.clone()
    }
}

impl Hash for Track {
    fn hash<H: Hasher>(&self, state: &mut H) {
        return self.file_path.hash(state);
    }
}

impl PartialEq for Track {
    fn eq(&self, other: &Self) -> bool {
        return self.file_path == other.file_path;
    }
}

impl Eq for Track {
}

pub struct Rotation {
    id: u32,
    tracks: IndexSet<Arc<Track>>,
}

impl Rotation {
    pub fn add(&mut self, track: Arc<Track>) {
        self.tracks.insert(track);
    }

    pub fn number_of_tracks(&self) -> usize {
        return self.tracks.len();
    }

    pub fn random_track(&self) -> Arc<Track> {
        let track_no = rand::thread_rng().gen_range(0, self.tracks.len());
        return Arc::clone(&self.tracks[track_no]);
    }

    pub fn remove(&mut self, track: &Arc<Track>) {
        self.tracks.remove(track);
    }
}

pub struct Catalog {
    tracks: Vec<Arc<Track>>,
    jamendo: HashMap<u32, u32>,
    default_rotation: Rotation,
    persistence: Arc<sqlite::Sqlite>,
}

#[derive(Debug)]
pub enum CatalogError {
    CannotOpenDatabase,
    CannotReadDatabase,
    CannotWriteDatabase,
}

impl Catalog {
    pub fn add_track_to_catalog_and_rotation(&mut self, artist: String, title: String, file: String) -> Result<(), CatalogError> {
        let track = self.add_new_track(artist, title, file)?;
        let conn = match Connection::open(&self.persistence.db_path) {
            Ok(c) => c,
            Err(_) => { return Err(CatalogError::CannotOpenDatabase) },
        };
        match conn.execute(
            "insert or ignore into RotationTracks (rotation_id, track_id) values (?1, ?2)",
            &[&self.default_rotation.id, &track.id]) {
            Ok(_) => (),
            Err(_) => { return Err(CatalogError::CannotWriteDatabase) },
        };
        self.default_rotation.add(track);
        Ok(())
    }

    pub fn add_new_track(&mut self, artist: String, title: String, file: String) -> Result<Arc<Track>, CatalogError> {
        let conn = match Connection::open(&self.persistence.db_path) {
            Ok(c) => c,
            Err(_) => { return Err(CatalogError::CannotOpenDatabase) },
        };
        match conn.execute(
            "insert or ignore into tracks (artist, title, file) values (?1, ?2, ?3)",
            &[&artist, &title, &file]) {
            Ok(_) => (),
            Err(_) => { return Err(CatalogError::CannotWriteDatabase) },
        };
        let mut stmt = match conn.prepare(
            "select id from tracks where file=?1"
        ) {
            Ok(s) => s,
            Err(_) => { return Err(CatalogError::CannotReadDatabase) },
        };
        let id = match stmt.query_row(&[&file], |row|
            Ok(row.get(0)?)) {
            Ok(n) => n,
            Err(_) => { return Err(CatalogError::CannotReadDatabase) },
        };

        Ok(self.add_track(Track {
                id,
                artist,
                title,
                file_path: file,
                jamendo_id: 0
            }))
    }

    fn add_track(&mut self, track: Track) -> Arc<Track> {
        let result = Arc::new(track);
        self.tracks.push(Arc::clone(&result));
        return result;
    }

    pub fn remove_track_from_rotation(&mut self, track: &Arc<Track>) -> Result<(), CatalogError> {
        let conn = match Connection::open(&self.persistence.db_path) {
            Ok(c) => c,
            Err(_) => { return Err(CatalogError::CannotOpenDatabase) },
        };
        match conn.execute(
            "delete from RotationTracks where rotation_id=1 and track_id=?1",
            &[track.id]) {
            Ok(_) => (),
            Err(_) => { return Err(CatalogError::CannotReadDatabase) },
        };
        self.default_rotation.remove(track);
        Ok(())
    }

    pub fn contains_jamendo_track(&self, id: u32) -> bool {
        for track in &self.tracks {
            if track.jamendo_id == id {
                return true;
            }
        }
        return false;
    }

    pub fn random_track(&self) -> Arc<Track> {
        return self.default_rotation.random_track();
    }

    pub fn new(db_path: String) -> Catalog {
        let mut catalog = Catalog {
            tracks: Vec::new(),
            jamendo: HashMap::new(),
            default_rotation: Rotation { id: 1, tracks: IndexSet::new() },
            persistence: Arc::new(sqlite::Sqlite::new(db_path)),
        };
        catalog.init_db().expect("Problem initializing db. Aborting!");
        return catalog; 
    }

    fn init_db(&mut self) -> Result<(), Box<dyn Error>> {
        let conn = Connection::open(&self.persistence.db_path)?;
        let mut stmt = conn.prepare(
            "select id, artist, title, file from tracks;"
        )?;


        let tracks = stmt.query_map(NO_PARAMS, |row|
            Ok(Track {
                id: row.get(0)?,
                artist: row.get(1)?,
                title: row.get(2)?,
                file_path: row.get(3)?,
                jamendo_id: 0,
            }));

        let mut track_by_id = HashMap::new();
        for track in tracks? {
            let t = self.add_track(track?);
            track_by_id.insert(t.id, t);
        }

        let mut jamendo_stmt = conn.prepare(
            "select jamendo_id, track_id from JamendoTracks;"
        )?;
        let jamendo_rows = jamendo_stmt.query_map(NO_PARAMS, |row| Ok((row.get(0)?, row.get(1)?)));
        
        self.jamendo = HashMap::new();
        for row in jamendo_rows? {
            let (key, value) = row?;
            self.jamendo.insert(key, value);
        }

        let mut rotation_stmt = conn.prepare(
            "select track_id from RotationTracks where rotation_id=1"
        )?;
        let rotation_rows = rotation_stmt.query_map(NO_PARAMS, |row| 
            Ok(
                Arc::clone(track_by_id.get(&(row.get(0)?)).unwrap())
            )
        );
        for track in rotation_rows? {
            self.default_rotation.add(track?);
        }

        return Ok(());
    }

    pub fn number_of_tracks(&self) -> usize {
        return self.tracks.len();
    }

    pub fn write_to_db(&mut self) {
        let conn = Connection::open(&self.persistence.db_path).expect("Cannot open catalog file");
        for (jamendo_id, track_id) in &self.jamendo {
             conn.execute(
                "insert or ignore into jamendo_tracks (track_id, jamendo_id) values (?1, ?2)",
                &[track_id, jamendo_id]).expect("Problem inserting");
        }
    }

    pub fn tracks(&self) -> &Vec<Arc<Track>>{
        return &self.tracks;
    }

    pub fn rotation_size(&self) -> usize {
        return self.tracks.len();
    }

    pub fn get_default_rotation(&mut self) -> &mut Rotation {
        return &mut self.default_rotation;
    }
}


pub fn init_catalog(file_name: String) -> Catalog {
    let mut catalog = Catalog::new(file_name);
    catalog.write_to_db();
    return catalog;
}

pub trait Persistence {
    fn load_tracks(&mut self) -> Result<Vec<Arc<Track>>, String>;
}


#[cfg(test)]
mod tests {
    use std::fs;

    fn add_one_track(catalog: &mut super::Catalog) -> Result<(), super::CatalogError> {
        catalog.add_track_to_catalog_and_rotation("Josh Woodward".to_string(), "Only Dreaming".to_string(), "dreaming.mp3".to_string())?;
        Ok(())
    }

    fn add_two_tracks(catalog: &mut super::Catalog) -> Result<(), super::CatalogError> {
        catalog.add_track_to_catalog_and_rotation("Tryad".to_string(), "Final Rewind".to_string(), "rewind.mp3".to_string())?;
        catalog.add_track_to_catalog_and_rotation("E.F.T.B.".to_string(), "What You Get".to_string(), "whatyouget.mp3".to_string())?;
        Ok(())
    }


    #[test]
    fn add_track_to_catalog() -> Result<(), super::CatalogError> {
        fs::remove_file("tests/temp/unit_test_catalog.db".to_string()).ok();
        let mut catalog = super::init_catalog("tests/temp/unit_test_catalog.db".to_string());
        assert_eq!(catalog.number_of_tracks(), 0);
        add_one_track(&mut catalog)?;
        assert_eq!(catalog.number_of_tracks(), 1);
        add_two_tracks(&mut catalog)?;
        assert_eq!(catalog.number_of_tracks(), 3);
        Ok(())
    }

    #[test]
    fn add_and_remove_track_from_rotation() -> Result<(), super::CatalogError> {
        fs::remove_file("tests/temp/unit_test2_catalog.db".to_string()).ok();
        let mut catalog = super::init_catalog("tests/temp/unit_test2_catalog.db".to_string());
        add_one_track(&mut catalog)?;
        let track = catalog.random_track();
        assert_eq!(track.artist, "Josh Woodward".to_string());
        add_two_tracks(&mut catalog)?;
        assert_eq!(catalog.get_default_rotation().number_of_tracks(), 3);
        catalog.remove_track_from_rotation(&track)?;
        assert_eq!(catalog.get_default_rotation().number_of_tracks(), 2);
        assert_eq!(catalog.number_of_tracks(), 3);
        Ok(())
    }


    #[test]
    fn write_and_read_new_catalog() -> Result<(), super::CatalogError> {
        fs::remove_file("tests/temp/persistency/catalog.db".to_string()).ok();
        {
            let mut catalog = super::init_catalog("tests/temp/persistency/catalog.db".to_string());
            assert_eq!(catalog.number_of_tracks(), 0);
            add_one_track(&mut catalog)?;
            add_two_tracks(&mut catalog)?;
            catalog.add_new_track("Professor Kliq".to_string(), "Pangea".to_string(), "pangea.mp3".to_string())?;
            assert_eq!(catalog.number_of_tracks(), 4);
            assert_eq!(catalog.get_default_rotation().number_of_tracks(), 3);
            catalog.remove_track_from_rotation(&catalog.random_track())?; 
            catalog.write_to_db();
        }
        {
            let mut catalog = super::init_catalog("tests/temp/persistency/catalog.db".to_string());
            assert_eq!(catalog.number_of_tracks(), 4);
            let rotation = catalog.get_default_rotation();
            assert_eq!(rotation.number_of_tracks(), 2);
        }
        fs::remove_file("tests/temp/persistency/catalog.db".to_string()).ok();
        Ok(())
    }

    #[test]
    fn write_reload_add_again_and_reload() -> Result<(), super::CatalogError> {
        fs::remove_file("tests/temp/persistency/catalog2.db".to_string()).ok();
        {
            let mut catalog = super::init_catalog("tests/temp/persistency/catalog2.db".to_string());
            assert_eq!(catalog.get_default_rotation().number_of_tracks(), 0);
            add_two_tracks(&mut catalog)?;
            assert_eq!(catalog.get_default_rotation().number_of_tracks(), 2);
            catalog.write_to_db();
        }
        {
            let mut catalog = super::init_catalog("tests/temp/persistency/catalog2.db".to_string());
            assert_eq!(catalog.get_default_rotation().number_of_tracks(), 2);
            add_one_track(&mut catalog)?;
            assert_eq!(catalog.get_default_rotation().number_of_tracks(), 3);
            catalog.write_to_db();
        }
        {
            let mut catalog = super::init_catalog("tests/temp/persistency/catalog2.db".to_string());
            assert_eq!(catalog.number_of_tracks(), 3);
            assert_eq!(catalog.get_default_rotation().number_of_tracks(), 3);
        }
        fs::remove_file("tests/temp/persistency/catalog2.db".to_string()).ok();
        Ok(())
    }

    #[test]
    fn read_old_catalogs() {
        let catalog = super::init_catalog("tests/resources/persistency/catalog20200920.db".to_string());
        assert_eq!(catalog.number_of_tracks(), 3);
    }

}
