use std::error::Error;
use std::sync::Arc;

use rusqlite::{Connection};
use rusqlite::NO_PARAMS;

use super::catalog::{Track};

pub struct Sqlite {
    pub db_path: String    
}

impl Sqlite {
    pub fn new(db_path: String) -> Sqlite {
        let mut result = Sqlite {
            db_path,
        };
        result.init_db().expect("Problem initializing db. Aborting!");
        result 
    }

    fn init_db(&mut self) -> Result<(), Box<dyn Error>> {
        let conn = Connection::open(&self.db_path)?;
        conn.execute(
            "create table if not exists Tracks(
                 id integer primary key,
                 artist varchar not null,
                 title varchar not null,
                 file varchar not null,
                 constraint file unique(file)
             )",
            NO_PARAMS,
        )?;
        conn.execute(
            "create table if not exists JamendoTracks(
                 jamendo_id integer primary key,
                 track_id integer,
                 constraint track unique(track_id)
             )",
            NO_PARAMS,
        )?;
        conn.execute(
            "create table if not exists RotationTracks (
                 rotation_id integer,
                 track_id integer,
                 constraint relation unique(rotation_id, track_id)
             )",
            NO_PARAMS,
        )?;
        return Ok(());
    }
}


impl super::catalog::Persistence for Sqlite {
    fn load_tracks(&mut self) -> Result<Vec<Arc<Track>>, String> {
        let mut result = Vec::new();
        let conn = Connection::open(&self.db_path).map_err(|_| "Can't open db")?;
        let mut stmt = conn.prepare(
            "select id, artist, title, file from tracks;"
        ).map_err(|_| "Can't read from db")?;


        let tracks = stmt.query_map(NO_PARAMS, |row|
            Ok(Arc::new(Track::new(
                row.get(0)?,
                row.get(1)?,
                row.get(2)?,
                row.get(3)?,
            ))));

        for track in tracks.map_err(|_| "Can't read from db")? {
            result.push(track.map_err(|_| "Can't read from db")?);
        };
        Ok(result)
    }
}

