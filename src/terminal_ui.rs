use std::io::{stdin, stdout};
use std::sync::{Arc, Mutex};
use std::thread;
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use tokio::sync::mpsc;

use super::application::{JamendoTrackInfo, UserInterfaceContext, UserInterfaceControl};
use super::catalog::{Track};


fn print_init_message() {
    println!("Starting ViralFM.\r");
    println!("Thanks for testing the alpha version :D\r");
    println!("Press h for help!\r");
}

fn print_help_message() {
    println!("Press a key. Your options are:\r");
    println!(" d - drop current song out of rotation and play next song\r");
    println!(" h - print this message\r");
    println!(" n - play next song\r");
    println!(" q - quit\r");
    println!(" <space> - pause/unpause song in progress\r");
}
pub struct TerminalRadioUI {
    keys: AsyncKeys,
}

pub struct TerminalControl {
}

impl TerminalRadioUI { 
    pub fn new_events_and_control() -> (TerminalRadioUI, TerminalControl) {
        print_init_message();
        return (
          TerminalRadioUI {
              keys: AsyncKeys::new(),
          },
          TerminalControl {
          }    
        )
    }

    pub async fn run<C: UserInterfaceContext + Send> (&mut self, context: Arc<Mutex<C>>) {
        loop {
            match self.keys.next().await {
                Key::Char('d') => { 
                    context.lock().unwrap().drop_current_track();
                    return;
                }
                Key::Char('h') => { print_help_message(); }
                Key::Char('n') => { 
                    context.lock().unwrap().play_next_track();
                    return; 
                }
                Key::Char(' ') => { 
                    context.lock().unwrap().pause_or_unpause(); 
                    return; 
                }
                Key::Char('q') => { 
                    context.lock().unwrap().quit(); 
                    return; 
                }
                Key::Char('?') => { print_help_message(); }
                _              => { }
            }
        }
    }
} 

impl UserInterfaceControl for TerminalControl {
    fn downloading_file(&self, track_info: &JamendoTrackInfo, full_path: &String) {
        println!("Downloading file from {} to {}\r", track_info.url, full_path); 
    }

    fn now_playing(&self, track: &Track) {
        print!("Now playing: {}\r\n", track.display());
    }
}

#[allow(dead_code)]
struct AsyncKeys {
    rx: mpsc::UnboundedReceiver<Key>,
    input_handle: thread::JoinHandle<()>,
    _stdout: termion::raw::RawTerminal<std::io::Stdout>,
}

#[warn(dead_code)]
impl AsyncKeys {
    fn new() -> AsyncKeys {
        let (tx, rx) = mpsc::unbounded_channel();
        let input_handle = {
            let tx = tx.clone();
            thread::spawn(move || {
                let stdin = stdin();
                for evt in stdin.keys() {
                    tx.send(evt.unwrap()).unwrap();
                }
            })
        };
        let _stdout = stdout().into_raw_mode().unwrap();
        AsyncKeys {
            rx,
            input_handle,
            _stdout,
        }
    }

    async fn next(&mut self) -> Key {
        loop {
            match self.rx.recv().await {
                Some(key)   => { return key; },
                None        => { },
            }
        }
    }

}

