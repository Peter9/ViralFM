use viralfm::application::{ApplicationContext, Downloader, PlayerCallback, PlayerControl, UserInterfaceControl};
use viralfm::jamendo::{JamendoDownloaderEvents, JamendoDownloader};
use viralfm::terminal_ui::TerminalRadioUI;
use viralfm::rodio_player::{RodioPlayerControl, RodioPlayerEvents};
use viralfm::catalog;

use std::sync::{Arc, Mutex};
use async_trait::async_trait;


fn main() {
    let (mut player_events, player_control) = RodioPlayerControl::new_events_and_control();
    let (mut ui, ui_control) = TerminalRadioUI::new_events_and_control();
    let (mut downloader_events, downloader) = JamendoDownloader::new_events_and_control();
    let application = Application::new(&mut player_events, Box::new(player_control), &mut ui, Box::new(ui_control), &mut downloader_events, Box::new(downloader));
    application.run();
}

#[async_trait]
pub trait PlayerEvents {
    async fn run<C: PlayerCallback> (&mut self, callback: Arc<Mutex<C>>);
}

pub struct Application<'a> {
    context: Arc<Mutex<ApplicationContext>>,
    player_events: &'a mut RodioPlayerEvents,
    downloader_events: &'a mut JamendoDownloaderEvents,
    radio_ui: &'a mut TerminalRadioUI, 
}

fn init_catalog() -> catalog::Catalog {
    let mut path_buf = dirs::data_dir().unwrap();
    path_buf.push("viralfm");
    std::fs::create_dir_all(path_buf.as_path()).expect("Couldn't create music directory");
    path_buf.push("catalog.db");
    catalog::init_catalog(path_buf.to_str().unwrap().to_string())
}

fn get_music_directory() -> String {
    let mut path_buf = dirs::home_dir().unwrap();
    path_buf.push("viralfm");
    std::fs::create_dir_all(path_buf.as_path()).expect("Couldn't create music directory");
    path_buf.to_str().unwrap().to_string()
}

impl Application<'_> {
        pub fn new<'a>(player_events: &'a mut RodioPlayerEvents, player_control: Box<dyn PlayerControl>, radio_ui: &'a mut TerminalRadioUI, ui_control: Box<dyn UserInterfaceControl>, downloader_events: &'a mut JamendoDownloaderEvents , downloader: Box<dyn Downloader>)-> Application<'a> {
            let catalog = init_catalog();
            let music_dir = get_music_directory();

            let context = Arc::new(Mutex::new(ApplicationContext {
                catalog,
                current: None,
                running: true,
                downloading: false,
                player_control,
                ui_control,
                downloader,
                music_dir,
            }));
            Application {
                context,
                player_events,
                downloader_events,
                radio_ui,
            }
    }

    pub fn run(self) {
        let runtime = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .unwrap();
        runtime.block_on(async {
            (*self.context).lock().unwrap().play_random_track();
            while (*self.context).lock().unwrap().running {
                tokio::select! {
                    _ = self.radio_ui.run(Arc::clone(&self.context)) => {},
                    _ = self.player_events.run(Arc::clone(&self.context)) => {},
                    _ = self.downloader_events.run(Arc::clone(&self.context)) => {},
                }
            }
        });
        (*self.context).lock().unwrap().player_control.quit(); 
    }

}

