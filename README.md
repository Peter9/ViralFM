# ViralFM

**ViralFM** is a *decentralised* web radio that continues to play when you are offline.

## How does it work

The application emulates the workings of a Top100 radio station by downloading Free Music songs from the internet 
and putting them into heavy rotation from which it plays songs in random order.
Like in a radio station the user doesn't have a lot of control over the playback, but can skip a song or drop it from rotation
altogether. If a song is dropped the app will automatically try to download new songs, so that you don't run out of music.

## Why should I use it?

I wrote it mostly for myself, since I am really enjoying Free Music, that is music that I am allowed to download and share with my friends, and there is a lot of great music
out there. Unfortunately finding the music I really like is a lot of work, which is why I decided to write this little program to streamline the process.

## I have feedback/a feature request/a bug report

There is an issue tracker on codeberg and you can find me on mastodon: peter@mastodon.nl

## What about the viral part?

The idea is to automatically share the songs you don't totally dislike on social media, so that other users can pick up popular songs from their friends or internet-friends.
Until this minimal viral product is implemented, all you get is a music player that plays music you didn't ask for.

ENJOY!
